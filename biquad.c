/**
 * @file
 * @author Martin Stejskal
 * @brief Implementation of biQuad filter
 */
// ===============================| Includes |================================
#include "biquad.h"

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
// ================================| Defines |================================
// In some cases, the "M_PI" is not defined in math.h. So let's define it in
// such cases (simple workaround)
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif  // M_PI

#ifdef BIQUAD_DEBUG
// Define debug message format (simply use printf)
#define BQ_PRINTF(...) printf(__VA_ARGS__)
#else
// Debug is disabled
#define BQ_PRINTF(...)
#endif  // BIQUAD_DEBUG

#ifdef NDEBUG
// Release  - get rid of asserts
#define BQ_ASSERT(...)
#else
// Debug - probably want to keep asserts
#define BQ_ASSERT(...) assert(__VA_ARGS__)
#endif  // NDEBUG
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Convert float handler that all coefficients will be positive
 *
 * Having only positive coefficients (absolute value) can be helpful when
 * need to compare against some thresholds
 *
 * @param[in] p_original_filter_handler Input filter handler
 * @param[out] p_abs_filter_handler Output filter handler holding only
 * absolute values
 */
static void _get_abs_float_filter_handler(
    const biquad_filter_handler_float_t *const p_original_filter_handler,
    biquad_filter_handler_float_t *p_abs_filter_handler);

static void _scale_int_filter_vars(int32_t x_var, int8_t x_bit_shift,
                                   int32_t y_var, int8_t y_bit_shift,
                                   int32_t *p_x_scaled, int32_t *p_y_scaled,
                                   int8_t *p_scaled_bit_shift,
                                   bool scale_x_y_to_16_bit);

static void _scale_down_x(int32_t *p_x_var, int8_t scale_up_bit_shift,
                          int8_t *p_shifts_down);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
void filter_biquad_get_coef_float(
    filter_type_t type, float sample_freq_hz, float cutoff_freq_hz,
    float q_factor, biquad_filter_handler_float_t *const p_coeff) {
  // Default clean up "memory" coefficients
  p_coeff->mem.z1 = 0;
  p_coeff->mem.z2 = 0;

  // I have no idea how to name this parameter. It is simply called "k". Deal
  // with it
  float k_param = tan(M_PI * cutoff_freq_hz / sample_freq_hz);

  // Normalization parameter
  float norm_param = 1 / (1 + k_param / q_factor + k_param * k_param);

  switch (type) {
    case FILTER_TYPE_LOWPASS:
      p_coeff->coef.a0 = k_param * k_param * norm_param;
      p_coeff->coef.a1 = 2 * p_coeff->coef.a0;
      p_coeff->coef.a2 = p_coeff->coef.a0;
      p_coeff->coef.b1 = 2 * (k_param * k_param - 1) * norm_param;
      p_coeff->coef.b2 =
          (1 - k_param / q_factor + k_param * k_param) * norm_param;
      break;

    case FILTER_TYPE_HIGHPASS:
      p_coeff->coef.a0 = 1 * norm_param;
      p_coeff->coef.a1 = -2 * p_coeff->coef.a0;
      p_coeff->coef.a2 = p_coeff->coef.a0;
      p_coeff->coef.b1 = 2 * (k_param * k_param - 1) * norm_param;
      p_coeff->coef.b2 =
          (1 - k_param / q_factor + k_param * k_param) * norm_param;
      break;

    case FILTER_TYPE_BANDPASS:
      p_coeff->coef.a0 = k_param / q_factor * norm_param;
      p_coeff->coef.a1 = 0;
      p_coeff->coef.a2 = -p_coeff->coef.a0;
      p_coeff->coef.b1 = 2 * (k_param * k_param - 1) * norm_param;
      p_coeff->coef.b2 =
          (1 - k_param / q_factor + k_param * k_param) * norm_param;
      break;

    case FILTER_TYPE_NOTCH:
      p_coeff->coef.a0 = (1 + k_param * k_param) * norm_param;
      p_coeff->coef.a1 = 2 * (k_param * k_param - 1) * norm_param;
      p_coeff->coef.a2 = p_coeff->coef.a0;
      p_coeff->coef.b1 = p_coeff->coef.a1;
      p_coeff->coef.b2 =
          (1 - k_param / q_factor + k_param * k_param) * norm_param;
      break;

    default:
      BQ_ASSERT(0);
  }
}

void filter_biquad_conv_float_to_int_handler(
    const biquad_filter_handler_float_t *const p_filter_float_handler,
    const biquad_filter_convert_handler_cfg_t *const p_cfg,
    biquad_filter_handler_int_t *const p_filter_int_handler) {
  // Pointers shall not be empty
  BQ_ASSERT(p_filter_float_handler);
  BQ_ASSERT(p_filter_int_handler);

  biquad_filter_convert_handler_cfg_t cfg;
  if (NULL == p_cfg) {
    // Configuration not defined. use default
    cfg.max_abs_a_coef = 1000;
    cfg.max_abs_b_coef = 30000;
  } else {
    // Copy configuration
    (void)memcpy(&cfg, p_cfg, sizeof(biquad_filter_convert_handler_cfg_t));
  }

  // Use absolute values in order to simply find optimal multiplier later
  biquad_filter_handler_float_t float_h_abs;

  _get_abs_float_filter_handler(p_filter_float_handler, &float_h_abs);

  // Bit shift, which will be used. Every bit shift is equal to 2x multiplier.
  // So bit shift 3 means 2x2x2 = 8x
  int8_t a_bit_shift = 0;
  int8_t b_bit_shift = 0;

  // Multipliers used for checking if still in range or not
  int32_t a_mul_to_be_checked = 1;
  int32_t b_mul_to_be_checked = 1;

  bool overshoot_max_values = 0;

  // Need to process separately for A and B
  while (!overshoot_max_values) {
    // Using bit shifts, which is equal to 2x multiplication
    a_mul_to_be_checked *= 2;

    // Calculate new coefficients and check if they overshoot threshold or not
    if (((float_h_abs.coef.a0 * a_mul_to_be_checked) > p_cfg->max_abs_a_coef) ||
        ((float_h_abs.coef.a1 * a_mul_to_be_checked) > p_cfg->max_abs_a_coef) ||
        ((float_h_abs.coef.a2 * a_mul_to_be_checked) > p_cfg->max_abs_a_coef)) {
      // Overshoot detected. Do not continue. Keep multipliers "as they are"
      overshoot_max_values = 1;
    } else {
      // Still fit in
      BQ_ASSERT(a_bit_shift < INT8_MAX);

      a_bit_shift++;
    }
  }

  overshoot_max_values = 0;

  while (!overshoot_max_values) {
    // Using bit shifts, which is equal to 2x multiplication
    b_mul_to_be_checked *= 2;

    // Calculate new coefficients and check if they overshoot threshold or not
    if (((float_h_abs.coef.b1 * b_mul_to_be_checked) > p_cfg->max_abs_b_coef) ||
        ((float_h_abs.coef.b2 * b_mul_to_be_checked) > p_cfg->max_abs_b_coef)) {
      // Overshoot detected. Do not continue. Keep multipliers "as they are"
      overshoot_max_values = 1;
    } else {
      // Still fit in
      BQ_ASSERT(b_bit_shift < INT8_MAX);

      b_bit_shift++;
    }
  }

  // Multipliers are known. Just take original float handler, apply multipliers
  // and convert to integers

  // The "z" parameters shall be zero in order to "reset" filter. The "a" and
  // "b" coefficients might be scaled and that could cause failure behavior
  p_filter_int_handler->mem.z1 = 0;
  p_filter_int_handler->mem.z2 = 0;
  p_filter_int_handler->mem.z1_bit_shifts = 0;
  p_filter_int_handler->mem.z2_bit_shifts = 0;
  p_filter_int_handler->mem.res_scaled = 0;
  p_filter_int_handler->mem.res_bit_shifts = 0;

  // Copy also bit shifts (multipliers)
  p_filter_int_handler->coef.a_bit_shift = a_bit_shift;
  p_filter_int_handler->coef.b_bit_shift = b_bit_shift;

  p_filter_int_handler->coef.a0 =
      p_filter_float_handler->coef.a0 * (float)(1 << a_bit_shift);
  p_filter_int_handler->coef.a1 =
      p_filter_float_handler->coef.a1 * (float)(1 << a_bit_shift);
  p_filter_int_handler->coef.a2 =
      p_filter_float_handler->coef.a2 * (float)(1 << a_bit_shift);

  p_filter_int_handler->coef.b1 =
      p_filter_float_handler->coef.b1 * (float)(1 << b_bit_shift);
  p_filter_int_handler->coef.b2 =
      p_filter_float_handler->coef.b2 * (float)(1 << b_bit_shift);
}

int32_t filter_biquad_float(const int32_t sample,
                            biquad_filter_handler_float_t *p_filter_handler) {
  // In order to get accurate results, need to have EVERYTHING in float!
  const float result =
      (((float)sample * p_filter_handler->coef.a0) + p_filter_handler->mem.z1);

  // Prints used only for debug
  BQ_PRINTF(
      "== Current settings ==\n"
      " a0: %f a1: %f a2: %f\n"
      " b1: %f b2: %f\n"
      " z1: %f z2: %f\n",
      p_filter_handler->coef.a0, p_filter_handler->coef.a1,
      p_filter_handler->coef.a2, p_filter_handler->coef.b1,
      p_filter_handler->coef.b2, p_filter_handler->mem.z1,
      p_filter_handler->mem.z2);

  BQ_PRINTF(
      "== Result calc ==\n"
      " sample: %d a0: %f z1: %f\n"
      " sample * a0: %f\n"
      "   result: %f\n",
      sample, sample * p_filter_handler->coef.a0, p_filter_handler->mem.z1,
      sample * p_filter_handler->coef.a0, result);

  p_filter_handler->mem.z1 = ((float)sample * p_filter_handler->coef.a1) +
                             p_filter_handler->mem.z2 -
                             (p_filter_handler->coef.b1 * result);

  BQ_PRINTF(
      "== Z1 calc ==\n"
      " sample * a1: %f\n"
      " z2: %f\n"
      " sample * a1 + z2: %f\n"
      " b1 * result: %f\n"
      "   z1: %f\n",
      sample * p_filter_handler->coef.a1, p_filter_handler->mem.z2,
      (sample * p_filter_handler->coef.a1) + p_filter_handler->mem.z2,
      p_filter_handler->coef.b1 * result, p_filter_handler->mem.z1);

  p_filter_handler->mem.z2 = ((float)sample * p_filter_handler->coef.a2) -
                             (p_filter_handler->coef.b2 * result);

  BQ_PRINTF(
      "== Z2 calc ==\n"
      " sample * a2: %f\n"
      " b2 * result: %f\n"
      "   z2: %f\n",
      sample * p_filter_handler->coef.a2, p_filter_handler->coef.b2 * result,
      p_filter_handler->mem.z2);

  // Conversion as last step
  return (int32_t)result;
}

int32_t filter_biquad_int(const int16_t sample,
                          biquad_filter_handler_int_t *ps_filter_handler) {
  // Note: do not check input in detail. This shall be fast and small function

  // Calculated optimal bit shift for actual operation. This variable is widely
  // reused
  int8_t optimal_bit_shift = 0;

  // ======================| result = (sample * a0) + z1 |====================
  // Need to scale components to the same "level"
  int32_t sample_mul_a0 = sample * ps_filter_handler->coef.a0;
  int32_t sample_mul_a0_scaled = 0;
  int32_t z1_scaled = 0;

  // Do not need 16 bit output, since only perform add
  _scale_int_filter_vars(
      sample_mul_a0, ps_filter_handler->coef.a_bit_shift,
      ps_filter_handler->mem.z1, ps_filter_handler->mem.z1_bit_shifts,
      &sample_mul_a0_scaled, &z1_scaled, &optimal_bit_shift, 0);

  ps_filter_handler->mem.res_scaled = sample_mul_a0_scaled + z1_scaled;
  ps_filter_handler->mem.res_bit_shifts = optimal_bit_shift;

  BQ_PRINTF("INT res: %f\n",
            ps_filter_handler->mem.res_scaled /
                (float)(1 << ps_filter_handler->mem.res_bit_shifts));

  // ================| z1 = (sample * a1) + z2 - (result * b1) |==============
  // Again is necessary to scale variable to the same "level"
  int32_t sample_mul_a1 = sample * ps_filter_handler->coef.a1;

  int32_t sample_mul_a1_scaled = 0;
  int32_t z2_scaled = 0;
  int32_t b1_scaled = 0;

  int8_t sample_mul_a1_plus_z2_bit_shift = 0;

  // Get scaled a1 and z2. Do not need 16 bit values, since only add numbers
  _scale_int_filter_vars(
      sample_mul_a1, ps_filter_handler->coef.a_bit_shift,
      ps_filter_handler->mem.z2, ps_filter_handler->mem.z2_bit_shifts,
      &sample_mul_a1_scaled, &z2_scaled, &sample_mul_a1_plus_z2_bit_shift, 0);

  int32_t sample_mul_a1_plus_z2 = sample_mul_a1_scaled + z2_scaled;

  // Get scaled result and b1
  int32_t res_scaled = 0;
  b1_scaled = 0;
  int8_t res_b1_opt_bit_shift = 0;

  // Require 16 bit variables as output, since multiplying
  _scale_int_filter_vars(
      ps_filter_handler->mem.res_scaled, ps_filter_handler->mem.res_bit_shifts,
      ps_filter_handler->coef.b1, ps_filter_handler->coef.b_bit_shift,
      &res_scaled, &b1_scaled, &res_b1_opt_bit_shift, 1);

  int32_t res_b1_scaled = res_scaled * b1_scaled;
  // Multiplication operation above -> multiply also bit shift
  res_b1_opt_bit_shift = res_b1_opt_bit_shift << 1;

  // Scale following expressions:
  //  * (sample * a1) + z2
  //  * result * b1
  // Only add. No need to scale down to 16 bit the output values
  _scale_int_filter_vars(sample_mul_a1_plus_z2, sample_mul_a1_plus_z2_bit_shift,
                         res_b1_scaled, res_b1_opt_bit_shift,
                         &sample_mul_a1_plus_z2, &res_b1_scaled,
                         &optimal_bit_shift, 0);
  ps_filter_handler->mem.z1 = sample_mul_a1_plus_z2 - res_b1_scaled;
  ps_filter_handler->mem.z1_bit_shifts = optimal_bit_shift;

  BQ_PRINTF("INT Z1: %f\n",
            ps_filter_handler->mem.z1 /
                (float)(1 << ps_filter_handler->mem.z1_bit_shifts));

  // ==================| z2 = (sample * a2) - (b2 * result) |===================
  int32_t sample_mul_a2 = sample * ps_filter_handler->coef.a2;

  int32_t b2_scaled = 0;
  res_scaled = 0;
  int8_t b2_res_opt_bit_shift = 0;

  // b2 * result
  // Need 16 bit output in order to avoid overflow
  _scale_int_filter_vars(
      ps_filter_handler->coef.b2, ps_filter_handler->coef.b_bit_shift,
      ps_filter_handler->mem.res_scaled, ps_filter_handler->mem.res_bit_shifts,
      &b2_scaled, &res_scaled, &b2_res_opt_bit_shift, 1);

  int32_t b2_mul_res_scaled = b2_scaled * res_scaled;
  // Multiplication, need to deal with bit shifts
  b2_res_opt_bit_shift = b2_res_opt_bit_shift << 1;

  int32_t sample_mul_a2_scaled = 0;

  // Scale following expressions:
  //  * (sample * a2)
  //  * (b2 * result)
  // Do not need 16 bit output variables, since only add
  _scale_int_filter_vars(sample_mul_a2, ps_filter_handler->coef.a_bit_shift,
                         b2_mul_res_scaled, b2_res_opt_bit_shift,
                         &sample_mul_a2_scaled, &b2_mul_res_scaled,
                         &optimal_bit_shift, 0);

  ps_filter_handler->mem.z2 = sample_mul_a2_scaled - b2_mul_res_scaled;
  // Multiplication -> deal with it
  ps_filter_handler->mem.z2_bit_shifts = optimal_bit_shift;

  BQ_PRINTF("INT Z2: %f\n",
            ps_filter_handler->mem.z2 /
                (float)(1 << ps_filter_handler->mem.z2_bit_shifts));

  // Return single value for user convenience
  return ps_filter_handler->mem.res_scaled >>
         ps_filter_handler->mem.res_bit_shifts;
}

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _get_abs_float_filter_handler(
    const biquad_filter_handler_float_t *const p_original_filter_handler,
    biquad_filter_handler_float_t *p_abs_filter_handler) {
  BQ_ASSERT(p_original_filter_handler);
  BQ_ASSERT(p_abs_filter_handler);

  // Copy filter and then check sign
  (void)memcpy(p_abs_filter_handler, p_original_filter_handler,
               sizeof(biquad_filter_handler_float_t));

  if (p_abs_filter_handler->coef.a0 < 0) {
    p_abs_filter_handler->coef.a0 = -p_abs_filter_handler->coef.a0;
  }
  if (p_abs_filter_handler->coef.a1 < 0) {
    p_abs_filter_handler->coef.a1 = -p_abs_filter_handler->coef.a1;
  }
  if (p_abs_filter_handler->coef.a2 < 0) {
    p_abs_filter_handler->coef.a2 = -p_abs_filter_handler->coef.a2;
  }
  if (p_abs_filter_handler->coef.b1 < 0) {
    p_abs_filter_handler->coef.b1 = -p_abs_filter_handler->coef.b1;
  }
  if (p_abs_filter_handler->coef.b2 < 0) {
    p_abs_filter_handler->coef.b2 = -p_abs_filter_handler->coef.b2;
  }
}

static void _scale_int_filter_vars(int32_t x_var, int8_t x_bit_shift,
                                   int32_t y_var, int8_t y_bit_shift,
                                   int32_t *p_x_scaled, int32_t *p_y_scaled,
                                   int8_t *p_scaled_bit_shift,
                                   bool scale_x_y_to_16_bit) {
  // Time critical function. Therefore no input check here

  // Simple average when comes to bit shift (find "middle")
  int8_t optimal_bit_shift = (x_bit_shift + y_bit_shift) >> 1;

  // An extra shift downs necessary in order to avoid overflow when shifting up
  int8_t shifts_down = 0;

  if (x_bit_shift > y_bit_shift) {
    // The "x" is "bigger" from bit shift point of view
    int8_t y_scale_up = optimal_bit_shift - y_bit_shift;

    // Make sure that scale up will not cause overflow
    _scale_down_x(&y_var, y_scale_up, &shifts_down);

    y_bit_shift -= shifts_down;

    optimal_bit_shift = y_bit_shift + y_scale_up;

    int8_t x_scale_down = x_bit_shift - optimal_bit_shift;

    *p_x_scaled = x_var >> x_scale_down;
    *p_y_scaled = y_var << y_scale_up;

  } else {
    // The "y" is "bigger" from bit shift point of view
    int8_t x_scale_up = optimal_bit_shift - x_bit_shift;

    // Make sure that scale up will not cause overflow
    _scale_down_x(&x_var, x_scale_up, &shifts_down);

    x_bit_shift -= shifts_down;

    optimal_bit_shift = x_bit_shift + x_scale_up;

    int8_t y_scale_down = y_bit_shift - optimal_bit_shift;

    *p_x_scaled = x_var << x_scale_up;
    *p_y_scaled = y_var >> y_scale_down;
  }

  *p_scaled_bit_shift = optimal_bit_shift;

  if (scale_x_y_to_16_bit) {
    // In order to make processing easier, let's use positive numbers only
    int32_t x_abs = *p_x_scaled;
    int32_t y_abs = *p_y_scaled;

    int8_t num_of_shifts_down = 0;

    if (x_abs < 0) {
      x_abs = -x_abs;
    }
    if (y_abs < 0) {
      y_abs = -y_abs;
    }

    while ((x_abs > INT16_MAX) || (y_abs > INT16_MAX)) {
      x_abs = x_abs >> 1;
      y_abs = y_abs >> 1;
      num_of_shifts_down++;
    }

    *p_scaled_bit_shift -= num_of_shifts_down;
    // Need to track sign. Take original values as sign reference
    if (*p_x_scaled < 0) {
      *p_x_scaled = -x_abs;
    } else {
      *p_x_scaled = x_abs;
    }

    if (*p_y_scaled < 0) {
      *p_y_scaled = -y_abs;
    } else {
      *p_y_scaled = y_abs;
    }
  }

  // Deal with sign. It might "drop" because of shifting. This is compiler and
  // platform dependent
  if ((x_var < 0) && (*p_x_scaled > 0)) {
    *p_x_scaled = -*p_x_scaled;
  }
  if ((y_var < 0) && (*p_y_scaled > 0)) {
    *p_y_scaled = -*p_y_scaled;
  }
}

static void _scale_down_x(int32_t *p_x_var, int8_t scale_up_bit_shift,
                          int8_t *p_shifts_down) {
  // Define boundaries for positive and negative range
  const int32_t max_positive = (1 << (30 - scale_up_bit_shift));
  const int32_t min_negative = -max_positive;

  *p_shifts_down = 0;

  if (*p_x_var >= 0) {
    // X positive
    while (*p_x_var > max_positive) {
      *p_x_var = *p_x_var >> 1;
      *p_shifts_down += 1;
    }
  } else {
    // X negative
    while (*p_x_var < min_negative) {
      *p_x_var = *p_x_var >> 1;
      *p_shifts_down += 1;
    }

    // Deal with sign
    if (*p_x_var > 0) {
      *p_x_var = -*p_x_var;
    }
  }
}
