/**
 * @file
 * @author Martin Stejskal
 * @brief Set of tests for verification "integer" filter accuracy
 *
 * Using integer instead of float is on some MCU must, due to performance
 * reasons. Therefore is necessary to make sure that accuracy is within some
 * limits in order to be sure that filter works as expected (error is small or
 * negligible)
 */
#ifndef __TEST_INT_FILTER_ACCURACY_H__
#define __TEST_INT_FILTER_ACCURACY_H__
// ===============================| Includes |================================

// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
void test_int_filter_accuracy(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __TEST_INT_FILTER_ACCURACY_H__
