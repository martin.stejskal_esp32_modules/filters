/**
 * @file
 * @author Martin Stejskal
 * @brief Helpful utilities for simplifying test routines
 */
// ===============================| Includes |================================
#include "test_utils.h"

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
// ================================| Defines |================================
/**
 * @brief Error code indicating "end of file" Byte
 */
#define _END_OF_FILE (-1)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
int32_t tu_get_i32_sample(FILE *p_file, bool *p_eof) {
  // Pointers shall not be empty
  assert(p_file);
  assert(p_eof);

  int32_t sample = 0;

  char *p_line = NULL;
  size_t size_length = 0;
  ssize_t err_code = -1;

  // Read one line
  err_code = getline(&p_line, &size_length, p_file);

  // Check if hit end of file or not
  if (err_code == _END_OF_FILE) {
    *p_eof = true;
  } else {
    *p_eof = false;

    // Convert string into integer
    sample = (int32_t)atoi(p_line);
  }

  return sample;
}

// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
