/**
 * @file
 * @author Martin Stejskal
 * @brief Helpful utilities for simplifying test routines
 */
#ifndef __TEST_UTILS_H__
#define __TEST_UTILS_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
/**
 * @brief Load 1 sample from given CSV file
 * @param[in, out] p_file Pointer to file handler
 * @param[out] p_eof Value is set once end of file is detected
 * @return Loaded sample as signed integer
 */
int32_t tu_get_i32_sample(FILE *p_file, bool *p_eof);

// ==========================| Low level functions |==========================

#endif  // __TEST_UTILS_H__
