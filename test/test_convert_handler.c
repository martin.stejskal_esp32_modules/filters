/**
 * @file
 * @author Martin Stejskal
 * @brief Tests for converting float handler to integer handler
 */
// ===============================| Includes |================================
#include "test_convert_handler.h"

#include "biquad.h"
#include "unity.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  biquad_filter_handler_float_t float_h;
  biquad_filter_convert_handler_cfg_t cfg;

  biquad_filter_handler_int_t expected_result;
} test_case_t;
// ===========================| Global variables |============================
static const test_case_t m_test_cases[] = {
    {.float_h = {.coef = {.a0 = 0.001576104482074459,
                          .a1 = 0.003152208964148918,
                          .a2 = 0.001576104482074459,
                          .b1 = -1.8846019550130062,
                          .b2 = 0.890906372941304}},
     .cfg = {.max_abs_a_coef = 1000, .max_abs_b_coef = 20000},
     .expected_result =
         {
             .coef = {.a_bit_shift = 18,
                      .a0 = 413,
                      .a1 = 826,
                      .a2 = 413,
                      .b_bit_shift = 13,
                      .b1 = -15438,
                      .b2 = 7298},
         }},
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void check_test_case(const test_case_t *const p_test_case);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void test_convert_handler(void) {
  for (uint16_t test_case_idx = 0;
       test_case_idx < (sizeof(m_test_cases) / sizeof(m_test_cases[0]));
       test_case_idx++) {
    check_test_case(&m_test_cases[test_case_idx]);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void check_test_case(const test_case_t *const p_test_case) {
  biquad_filter_handler_int_t res_int_h;

  filter_biquad_conv_float_to_int_handler(&p_test_case->float_h,
                                          &p_test_case->cfg, &res_int_h);

  // Expected, real
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.z1);
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.z2);
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.z1_bit_shifts);
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.z2_bit_shifts);
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.res_scaled);
  TEST_ASSERT_EQUAL_INT32(0, res_int_h.mem.res_bit_shifts);

  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.a_bit_shift,
                          res_int_h.coef.a_bit_shift);
  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.b_bit_shift,
                          res_int_h.coef.b_bit_shift);

  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.a0,
                          res_int_h.coef.a0);
  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.a1,
                          res_int_h.coef.a1);
  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.a2,
                          res_int_h.coef.a2);

  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.b1,
                          res_int_h.coef.b1);
  TEST_ASSERT_EQUAL_INT32(p_test_case->expected_result.coef.b2,
                          res_int_h.coef.b2);
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
