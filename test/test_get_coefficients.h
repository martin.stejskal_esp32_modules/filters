/**
 * @file
 * @author Martin Stejskal
 * @brief Test file for checking generating coefficients
 */
#ifndef __TEST_GET_COEFFICIENTS_H__
#define __TEST_GET_COEFFICIENTS_H__
// ===============================| Includes |================================

// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief High level function for testing "get coefficients" functionality
 */
void test_get_coefficients(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __TEST_GET_COEFFICIENTS_H__
