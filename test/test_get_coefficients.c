/**
 * @file
 * @author Martin Stejskal
 * @brief Test file for checking generating coefficients
 */
// ===============================| Includes |================================
#include "test_get_coefficients.h"

#include <assert.h>
#include <stddef.h>

#include "biquad.h"
#include "unity.h"
// ================================| Defines |================================
/**
 * @brief Define expected float accuracy
 *
 * @note Experimentally verified. Basically making sure that algorithm works in
 * general while accuracy is good enough
 *
 * @{
 */
#define _ACCEPTED_FLOAT_DELTA_A_PARAMS (0.000001)
#define _ACCEPTED_FLOAT_DELTA_B_PARAMS (0.000001)
/**
 * @}
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief Describe one test use case
 */
typedef struct {
  filter_type_t type;
  float sample_freq_hz;
  float cutoff_freq_hz;
  float q_factor;

  biquad_filter_handler_float_t expected_values;
} test_case_t;
// ===========================| Global variables |============================
/**
 * @brief List of test cases
 */
static const test_case_t m_test_cases[] = {
    {
        .type = FILTER_TYPE_LOWPASS,
        .sample_freq_hz = 100,
        .cutoff_freq_hz = 1,
        .q_factor = 0.7071,
        .expected_values =
            {
                .mem =
                    {
                        .z1 = 0,
                        .z2 = 0,
                    },
                .coef =
                    {
                        .a0 = 0.0009446914586925257,
                        .a1 = 0.0018893829173850514,
                        .a2 = 0.0009446914586925257,
                        .b1 = -1.911196288237583,
                        .b2 = 0.914975054072353,
                    },
            },
    },
    {
        .type = FILTER_TYPE_LOWPASS,
        .sample_freq_hz = 100,
        .cutoff_freq_hz = 2.25,
        .q_factor = 0.7071,
        .expected_values =
            {
                .mem =
                    {
                        .z1 = 0,
                        .z2 = 0,
                    },
                .coef =
                    {
                        .a0 = 0.00453621377422025,
                        .a1 = 0.0090724275484405,
                        .a2 = 0.00453621377422025,
                        .b1 = -1.8006434925118127,
                        .b2 = 0.8187883476086938,
                    },
            },
    },
    {
        .type = FILTER_TYPE_LOWPASS,
        .sample_freq_hz = 100,
        .cutoff_freq_hz = 7,
        .q_factor = 0.7071,
        .expected_values =
            {
                .mem =
                    {
                        .z1 = 0,
                        .z2 = 0,
                    },
                .coef =
                    {
                        .a0 = 0.036574754677828704,
                        .a1 = 0.07314950935565741,
                        .a2 = 0.036574754677828704,
                        .b1 = -1.3908921947801067,
                        .b2 = 0.5371912134914214,
                    },
            },
    },
    {
        .type = FILTER_TYPE_LOWPASS,
        .sample_freq_hz = 100,
        .cutoff_freq_hz = 20,
        .q_factor = 0.7071,
        .expected_values =
            {
                .mem =
                    {
                        .z1 = 0,
                        .z2 = 0,
                    },
                .coef =
                    {
                        .a0 = 0.2065712872626558,
                        .a1 = 0.4131425745253116,
                        .a2 = 0.2065712872626558,
                        .b1 = -0.3695259524151479,
                        .b2 = 0.19581110146577096,
                    },
            },
    },

};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void check_test_case(const test_case_t *const p_test_case);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void test_get_coefficients(void) {
  // Check all test cases. Simple as that :)
  for (uint16_t test_case_idx = 0;
       test_case_idx < (sizeof(m_test_cases) / sizeof(m_test_cases[0]));
       test_case_idx++) {
    check_test_case(&m_test_cases[test_case_idx]);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// Unity testing stuff. Do not need to add pre and post processing
void setUp(void) {}
void tearDown(void) {}
// ====================| Internal functions: high level |=====================
static void check_test_case(const test_case_t *const p_test_case) {
  // Internal check
  assert(NULL != p_test_case);

  biquad_filter_handler_float_t filter_h;

  filter_biquad_get_coef_float(p_test_case->type, p_test_case->sample_freq_hz,
                               p_test_case->cutoff_freq_hz,
                               p_test_case->q_factor, &filter_h);

  // Delta, expected, actual

  // Memory (z1, z2) shall be cleaned
  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_A_PARAMS, 0, filter_h.mem.z1);
  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_A_PARAMS, 0, filter_h.mem.z2);

  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_A_PARAMS,
                           p_test_case->expected_values.coef.a0,
                           filter_h.coef.a0);
  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_A_PARAMS,
                           p_test_case->expected_values.coef.a1,
                           filter_h.coef.a1);
  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_A_PARAMS,
                           p_test_case->expected_values.coef.a2,
                           filter_h.coef.a2);

  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_B_PARAMS,
                           p_test_case->expected_values.coef.b1,
                           filter_h.coef.b1);
  TEST_ASSERT_FLOAT_WITHIN(_ACCEPTED_FLOAT_DELTA_B_PARAMS,
                           p_test_case->expected_values.coef.b2,
                           filter_h.coef.b2);
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
