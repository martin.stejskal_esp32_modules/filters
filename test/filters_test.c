/**
 * @file
 * @author Martin Stejskal
 * @brief Main test file for filters
 */
// ===============================| Includes |================================
#include "test_convert_handler.h"
#include "test_get_coefficients.h"
#include "test_int_filter_accuracy.h"
#include "unity.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
int main(void) {
  UNITY_BEGIN();

  RUN_TEST(test_get_coefficients);
  RUN_TEST(test_convert_handler);
  RUN_TEST(test_int_filter_accuracy);

  return UNITY_END();
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
