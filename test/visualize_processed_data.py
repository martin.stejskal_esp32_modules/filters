#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
================================================
Simple tool for visualization processed results
================================================

Little helper when debugging things. Easier to see things in the graphical form
instead of browsing thousands of values manually

``author`` Martin Stejskal
"""
from pathlib import Path
import matplotlib.pyplot as plt

DATA_FOLDER = "test_output"
SKIP_SAMPLES = 0


def get_list_of_data_files() -> list:
    """
    Scan data directory and returns list of found files

    :return: List of found data file in "data folder"
    """
    this_script_path = Path(__file__)
    data_folder = Path(this_script_path.parent / DATA_FOLDER)

    file_list = []

    for file in data_folder.iterdir():
        file_list.append(file)

    return file_list


def get_data_from_file(file: Path) -> dict:
    """
    Load data from CSV file and parse them into dictionary

    :param file: Path to the file to be processed
    :return: loaded data as dictionary
    """
    with open(file, newline='\n') as file_h:
        lines = file_h.readlines()

    # Expect header + at least 1 row
    assert len(lines) > 2

    header_split = lines[0]
    header_split = header_split.replace('\n', '')
    header_split = header_split.split(',')

    # Get rid of extra spaces
    header = []
    for key in header_split:
        header.append(key.strip())

    # Returned data structure as dictionary
    data_structure = {}

    for item in header:
        # Every item name will have own array
        data_structure[item] = []

    # Now add data
    for line in lines[1:]:
        # Get rid of new line
        line = line.replace('\n', '')

        # Get individual numbers
        line = line.split(',')

        item_idx = 0
        for value in line:
            value_int = int(value)

            # Header key - same order as in header list
            key = header[item_idx]
            data_structure[key].append(value_int)
            item_idx += 1
        # /for all values in the row
    # /for all values

    return data_structure


def plot_data_file(file: Path, figure_number: int):
    """
    Plot required data file

    :param file: Path to the file
    :param figure_number: identified for figure
    :return: None
    """
    loaded_data = get_data_from_file(file)

    plt.figure(figure_number)

    for key in loaded_data:
        y = loaded_data[key][SKIP_SAMPLES:]
        x = range(len(y))

        plt.plot(x, y, label=key)

    plt.title(str(file.stem))
    plt.grid()
    plt.legend()


# Main function
if __name__ == "__main__":
    data_files = get_list_of_data_files()
    if len(data_files) == 0:
        print("No data for processing found :(")
        exit(1)

    figure_counter = 1

    for file in data_files:
        plot_data_file(file, figure_counter)
        figure_counter += 1

    # Display all graphs
    plt.show()
