/**
 * @file
 * @author Martin Stejskal
 * @brief Set of tests for verification "integer" filter accuracy
 *
 * Using integer instead of float is on some MCU must, due to performance
 * reasons. Therefore is necessary to make sure that accuracy is within some
 * limits in order to be sure that filter works as expected (error is small or
 * negligible)
 */
// ===============================| Includes |================================
#include "test_int_filter_accuracy.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "biquad.h"
#include "test_utils.h"
#include "unity.h"
// ================================| Defines |================================
/**
 * @brief Path to data to be processed
 *
 * Sampled data from real life application
 */
#define _BLOW (1)

#if _BLOW
#define CSV_INPUT_DATA \
  "test_data/10_1x_blow_middle_8x_oversampling_181hz_sampling_5hz_cut.csv"
#else
#define CSV_INPUT_DATA \
  "test_data/09_no_blow_8x_oversampling_181hz_sampling_5hz_cut.csv"
#endif

#define OUTPUT_FOLDER "test_output"

/**
 * @brief Multiplier constants
 *
 * In order to not make mistake in number of zeros, let's use macros below
 *
 * @{
 */
#define _1K (1000)
#define _10K (10000)
#define _100K (100000)
#define _1M (1000000)
/**
 * @}
 */

/**
 * @brief Number of samples to be ignored after filtering
 *
 * Due to the integer filter inaccuracy it is expected that very first filtered
 * samples will significantly differ. This tells how many first samples shall be
 * skipped when comparing float and integer implementation
 */
#define DO_NOT_COMPARE_FIRST_N_SAMPLES (10)

/**
 * @brief Min & max difference thresholds when compare float and integer method
 *
 * The float and integer implementation differs in accuracy. Based on used
 * input data, there are expected differences when comes to the output.
 *
 * @{
 */
#if _BLOW
#define FILTERED_DIFF_MIN (-7700)
#define FILTERED_DIFF_MAX (-7200)
#else
#define FILTERED_DIFF_MIN (-7500)
#define FILTERED_DIFF_MAX (-7300)
#endif
/**
 * @}
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  // Basic filter parameters
  filter_type_t type;
  float sample_freq_hz;
  float cutoff_freq_hz;
  float q_factor;

  // Tell from where data shall be read
  char *p_source_data;

  // Parameters for converter function - allow to play with accuracy
  biquad_filter_convert_handler_cfg_t conv_cfg;

} test_case_t;
// ===========================| Global variables |============================
static const test_case_t m_test_cases[] = {
    {
        .type = FILTER_TYPE_LOWPASS,
        .sample_freq_hz = 181,
        // Using different cut off, in order to make filter "less aggressive" so
        // float and integer implementation results will be more closer to each
        // other (typically applied some offset and "small" inaccuracy in gain)
        .cutoff_freq_hz = 50,
        .q_factor = BIQUAD_DEFAULT_Q,
        .p_source_data = CSV_INPUT_DATA,
        .conv_cfg =
            {
                .max_abs_a_coef = _100K,
                .max_abs_b_coef = _10K,
            },
    },
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void check_test_case(const uint16_t case_number,
                            const test_case_t *const p_test_case);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void test_int_filter_accuracy(void) {
  for (uint16_t test_case_idx = 0;
       test_case_idx < (sizeof(m_test_cases) / sizeof(m_test_cases[0]));
       test_case_idx++) {
    check_test_case(test_case_idx, &m_test_cases[test_case_idx]);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void check_test_case(const uint16_t case_number,
                            const test_case_t *const p_test_case) {
  FILE *p_input_data_file = fopen(p_test_case->p_source_data, "r");
  assert(p_input_data_file != NULL);

  // The 255 characters shall be enough
  char output_file_name_buffer[255];
  (void)sprintf(output_file_name_buffer,
                OUTPUT_FOLDER
                "/"
                "%02d___sampling_%f_hz___cut_off_%f_hz.csv",
                case_number, p_test_case->sample_freq_hz,
                p_test_case->cutoff_freq_hz);
  FILE *p_output_data_file = fopen(output_file_name_buffer, "w");
  assert(p_output_data_file != NULL);

  // Get ideal filter parameters
  biquad_filter_handler_float_t filter_float_h;

  filter_biquad_get_coef_float(p_test_case->type, p_test_case->sample_freq_hz,
                               p_test_case->cutoff_freq_hz,
                               p_test_case->q_factor, &filter_float_h);

  // Get filter parameters for integer implementation
  biquad_filter_handler_int_t filter_int_h;

  filter_biquad_conv_float_to_int_handler(
      &filter_float_h, &p_test_case->conv_cfg, &filter_int_h);

  bool end_of_file = 0;
  int32_t sample = 0;
  int32_t filtered_float = 0;
  int32_t filtered_int = 0;

  // Difference between filtered via float and integer biquad filter
  int32_t filtered_diff = 0;

  // Write header into file
  fprintf(p_output_data_file, "raw, filter float, filter int\n");

  uint32_t cnt = 0;
  bool float_and_int_differ_too_much = false;

  while (!end_of_file) {
    sample = tu_get_i32_sample(p_input_data_file, &end_of_file);

    if (end_of_file || (cnt >= 1000000)) {
      break;
    }

    filtered_float = filter_biquad_float(sample, &filter_float_h);

    filtered_int = filter_biquad_int(sample, &filter_int_h);

    // Get absolute difference between float and int filter
    filtered_diff = filtered_float - filtered_int;

    if (cnt >= DO_NOT_COMPARE_FIRST_N_SAMPLES) {
      if ((filtered_diff < FILTERED_DIFF_MIN) ||
          (filtered_diff > FILTERED_DIFF_MAX)) {
        printf("Filter diff: %d\n", filtered_diff);
        float_and_int_differ_too_much = true;
      }
    }

    // Write data into output file

    fprintf(p_output_data_file, "%d, %d, %d\n", sample, filtered_float,
            filtered_int);

    cnt++;
  }

  // Need to properly close files
  (void)fclose(p_input_data_file);
  (void)fclose(p_output_data_file);

  TEST_ASSERT(!float_and_int_differ_too_much);
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
