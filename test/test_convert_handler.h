/**
 * @file
 * @author Martin Stejskal
 * @brief Tests for converting float handler to integer handler
 */
#ifndef __TEST_CONVERTER_HANDLER_H__
#define __TEST_CONVERTER_HANDLER_H__
// ===============================| Includes |================================

// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
void test_convert_handler(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __TEST_CONVERTER_HANDLER_H__
