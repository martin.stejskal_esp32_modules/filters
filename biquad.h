/**
 * @file
 * @author Martin Stejskal
 * @brief Implementation of biQuad filter
 */
#ifndef __FILTERS_BIQUAD_H__
#define __FILTERS_BIQUAD_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================
/**
 * @brief Default quality factor for filters
 *
 * This is just hint for developer. This is typical value used in filters, but
 * it does not mean that you have to stick with it.
 *
 * If your filter is not stable, lower it. If you need more sharp
 * characteristic, increase it, but check it very well for stability.
 */
#define BIQUAD_DEFAULT_Q (0.7071)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  FILTER_TYPE_LOWPASS = 0,
  FILTER_TYPE_HIGHPASS,
  FILTER_TYPE_BANDPASS,
  FILTER_TYPE_NOTCH,

  FILTER_TYPE_MAX,
} filter_type_t;

typedef struct {
  // Filter coefficients. These define filter type and it's characteristics
  struct {
    float a0;
    float a1;
    float a2;

    float b1;
    float b2;
  } coef;

  // Intermediate results. Use by filter algorithm. It is kind of memory of
  // previous calculations
  struct {
    float z1;
    float z2;
  } mem;
} biquad_filter_handler_float_t;

typedef struct {
  // Filter coefficients. These define filter type and it's characteristics
  struct {
    // Decimal point logic. Tells how many bit shifts "to left" (2x) were made
    // for "A" coefficients
    int8_t a_bit_shift;
    int32_t a0;
    int32_t a1;
    int32_t a2;

    // Decimal point logic. Tells how many bit shifts "to left" (2x) were made
    // for "A" coefficients
    int8_t b_bit_shift;
    int32_t b1;
    int32_t b2;

  } coef;

  // Intermediate results. Use by filter algorithm. It is kind of memory of
  // previous calculations
  struct {
    // Set them to zero when initialize this structure
    int32_t z1;

    // Scale of z1 memory
    int8_t z1_bit_shifts;

    int32_t z2;
    // Scale of z2 memory
    int8_t z2_bit_shifts;

    // Store last calculated result, but scaled
    int32_t res_scaled;
    int8_t res_bit_shifts;
  } mem;
} biquad_filter_handler_int_t;

typedef struct {
  // Maximum acceptable value for A coefficients. This depends on every
  // application. Depends how "big" is data input. Typically value shall not
  // overshoot 1000. In case of B argument, value shall fit into 30 000.
  // Note that this is absolute value.

  // Rule of thumb: try to not overshoot 16 bit value unless you know what are
  // you doing
  int32_t max_abs_a_coef;
  int32_t max_abs_b_coef;
} biquad_filter_convert_handler_cfg_t;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Calculate filter coefficients based on input arguments
 * @param type Filter type
 * @param sample_freq_hz Sampling frequency in Hz
 * @param cutoff_freq_hz Cutoff frequency in Hz
 * @param q_factor Quality factor
 * @param[out] p_coeff Filter coefficients
 */
void filter_biquad_get_coef_float(filter_type_t type, float sample_freq_hz,
                                  float cutoff_freq_hz, float q_factor,
                                  biquad_filter_handler_float_t *const p_coeff);

/**
 * @brief Convert "float" handler to "integer" handler
 *
 * Convert float into integers and deal with multipliers. Super useful when
 * you're lazy and do not want to think much about rounding and other stuff
 *
 * @param[in] p_filter_float_handler Pointer to float handler
 * @param[in] p_cfg Pointer to configuration structure. If NULL is given,
 *                  default will be used. But keep in mind, that you can deal
 *                  with overflow issues or inaccurate results. So use default
 *                  wisely.
 * @param[out] p_filter_int_handler Write converted data here
 */
void filter_biquad_conv_float_to_int_handler(
    const biquad_filter_handler_float_t *const p_filter_float_handler,
    const biquad_filter_convert_handler_cfg_t *const p_cfg,
    biquad_filter_handler_int_t *const p_filter_int_handler);

/**
 * @brief Filter input value by using float
 *
 * This takes more CPU time, but it is more accurate than
 * @ref filter_biquad_int()
 *
 * @param sample Input sample value
 * @param p_filter_handler Pointer to the filter handler
 * @return Filtered value
 */
int32_t filter_biquad_float(const int32_t sample,
                            biquad_filter_handler_float_t *p_filter_handler);

/**
 * @brief Filter input value by using integer only
 *
 * Faster, but less accurate alternative to @ref filter_biquad_float().
 *
 * @param sample Input sample value
 * @param p_filter_handler Pointer to the filter handler
 * @return Filtered value
 */
int32_t filter_biquad_int(const int16_t sample,
                          biquad_filter_handler_int_t *p_filter_handler);
#endif  // __FILTERS_BIQUAD_H__
